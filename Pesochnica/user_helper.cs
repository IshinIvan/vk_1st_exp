﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pesochnica
{
    public class City
    {
        public int id;
        public string title;
    }

    public class Country
    {
        public int id;
        public string title;
    }

    public class user_helper
    {
        public int id;
        public string first_name;
        public string last_name;
        public City city;
        public string bdate;
        public string photo_id;
        public Country country;
    }
    public class Response
    {
        public user_helper[] response ;
    }
}
