﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pesochnica
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string s = textBox1.Text;
            string s_get_url = "users.get";
            string s_pr = "user_ids=" + textBox1.Text;
            string s_fields = "&fields=photo_max," + textBox2.Text;
            //Формирование и отправка Get запроса
            string s1 = Get_Method.GetClass.GET(vk_helper.s_url + s_get_url, s_pr + s_fields + vk_helper.s_vers);
            //Запись с разделением элементов (Json)
            try
            {
                Response user = JsonConvert.DeserializeObject<Response>(s1);
                textBox_id.Text = user.response[0].id.ToString();
                textBox_firstname.Text = user.response[0].first_name;
                textBox_lastname.Text = user.response[0].last_name;
                textBox_photoid.Text = user.response[0].photo_id.Replace("\\", "");
                //country
                bool f_country = textBox2.Text.Contains("country");
                if (f_country)
                {
                    textBox_fields_res.Text = textBox_fields_res.Text + user.response[0].country.title + " ";
                }
                //city
                bool f_city = textBox2.Text.Contains("city");
                if (f_city)
                {
                    textBox_fields_res.Text = textBox_fields_res.Text + user.response[0].city.title + " ";
                }
                //bdate
                bool f_bdate = textBox2.Text.Contains("bdate");
                if (f_bdate)
                {
                    textBox_bdate.Text = user.response[0].bdate;
                }
            }
            catch (Exception er)
            {
                Error user_error = JsonConvert.DeserializeObject<Error>(s1);
                if (user_error.error != null)
                {
                    textBox_id.Text = "User not found";
                }
            }
            //Изменение для представления без разделения элементов
            s1 = s1.Replace(',', '\n').Replace('[', '\n').Replace("{", "").Replace("}", "").Replace("]", "").Replace("\\", "");
            richTextBox1.Text = s1;
        }
    }
}
